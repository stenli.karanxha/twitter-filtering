#!/bin/bash
#
# Written by Stenli Karanxha <stenli_karanxha@yahoo.com>
#
# installs the extra modules needed for the project B in MAT101
#

tar -zxvf httplib2-0.8.tar.gz
cd httplib2-0.8
python setup.py install --user
cd ..
rm -r httplib2-0.8

tar -zxvf oauth2-1.5.211.tar.gz
cd oauth2-1.5.211
python setup.py install --user
cd ..
rm -r oauth2-1.5.211
