################################################################
#  Connection to the twitter server, downloading of the tweets, and pre-filtering based on presence of a
#  text, language and country.
################################################################

import oauth2 as oauth
import urllib2 as urllib
import json

"""
Output of the twitter downloading part: list of tweets, each of them being a tuple with the following composition:
(text, language, time, country, hashtags)
"""
tweet_list=[]

"""
Hardcoded definitions for the authentication to the twitter server.
"""
access_token_key = "1960885710-AhVlOKoBlYLxYDUFbhXA1DUF1rLYjnOlH8LH9a8"
access_token_secret = "cDlvhJAFjXx0Xr7Sjqi6YecmJdPwRmmGvPDKeTo7IE"
oauth_token    = oauth.Token(key=access_token_key, secret=access_token_secret)

consumer_key = "kHsWFVEOHBZEV9DY8eqg"
consumer_secret = "YrXdGvGjdYJChzfnd7cJqdZLT74F9cKLQvorltPTuY"
oauth_consumer = oauth.Consumer(key=consumer_key, secret=consumer_secret)

signature_method_hmac_sha1 = oauth.SignatureMethod_HMAC_SHA1()

_debug = 0
http_handler  = urllib.HTTPHandler(debuglevel=_debug)
https_handler = urllib.HTTPSHandler(debuglevel=_debug)

http_method = "GET"

def get_twitter_response(url):
    """Construct, sign, and open a twitter request using the hard coded credentials. 
    """
    req = oauth.Request.from_consumer_and_token(oauth_consumer,
                                             token=oauth_token,
                                             http_method=http_method,
                                             http_url=url, 
                                             parameters=[])
    req.sign_request(signature_method_hmac_sha1, oauth_consumer, oauth_token)
    headers = req.to_header()

    encoded_post_data = None
    url = req.to_url()
    
    opener = urllib.OpenerDirector()
    opener.add_handler(http_handler)
    opener.add_handler(https_handler)
    response = opener.open(url, encoded_post_data)
    return response


"""
Definitions of keys and values of the relevant fields of a tweet.
"""
key_lang = "lang"
val_german = "de"
key_text = "text"
key_place = "place"
key_country_code = "country_code"
val_CH= "CH"
key_full_name = "full_name"

def is_tweet_valid(tweet):
    """Criteria of validity of the tweets.
        Only accepts tweets in german, that contain a text.
        The country check is disabled because apparently no tweets 
        with this information were received.
    """
    return key_lang in tweet \
           and tweet[key_lang] == val_german \
           and key_text in tweet 
#           and key_place in tweet \
#           and type(tweet[key_place]) == dict \
#           and key_country_code in tweet[key_place] \
#           and tweet[key_place][key_country_code] == val_CH

"""
Hardcoded url of the twitter stream-server
"""
url = "https://stream.twitter.com/1/statuses/sample.json"

def fetch_tweets(maxTweets):
    """Connects to the stream server, gets the tweets that fill the criteria
        of validity.
    """
    response = get_twitter_response(url)
    count = 0
    for line in response:
        message = json.loads(line)
        if is_tweet_valid(message):
            count = count + 1
            if count == maxTweets:
                break
            else:
                text = message[key_text]
                language = message[key_lang]
                time= None
                country=None
                hashtags=[]
                cleaned_tweet = (text, language, time, country, hashtags)
                tweet_list.append(cleaned_tweet)
                print cleaned_tweet


################################################################
# Filter functions
# This functions filter a list of my_tweet and make some statistics.
# my_tweet is a tuple (tweet, myLanguage, myTime, myGeographicIndication, [hashtags])
# tweet is of type string, myLanguage an enumerate, myTime a datetime object,
# myGeographicIndication a location and [hashtags] a list of strings
################################################################

from datetime import datetime as datetime
from datetime import date as date

def statistic(words_of_search, list_of_tweets):
       """statistic(words_of_search, list_of_tweets) -> list, int

       words_of_search is a list of strings, list_of_tweets a list of my_tweet
       The function compares the len of two lists of tweets, the original one and one obtained from the
       function find_words. The return values are the filtered_list and the hit_rate"""
       filtered_list = filter(lambda my_tweet: find_words(my_tweet,words_of_search),list_of_tweets)
       hit_rate = 0
       if (len(list_of_tweets) != 0):
              hit_rate = len(filtered_list)*1./len(list_of_tweets)
       return filtered_list, hit_rate

def find_words(my_tweet, words_of_search):
       """find_words(my_tweet, words_of_search) -> bool

       words_of_search is a list of strings, list_of_tweets a list of my_tweet
       The function compares the len of two lists of tweets, the original one and one obtained from the
       function find_words"""
       upper_tweet = my_tweet[0].upper()
       return any(word.upper() in upper_tweet for word in words_of_search) #true if any word in words_of_search appears in my_tweet

def find_language(my_tweet, language_of_search):
       """tests if the language is the searched one"""
       language = my_tweet[1]
       return language == language_of_search

def find_date(my_tweet, dates_of_search):
    """tests if my_tweet is in a given dateperiod"""
    my_datetime = my_tweet[2]
    if not my_datetime == None:
        my_date = date(my_datetime.year, my_datetime.month, my_datetime.day)
        return any(searchdate == my_date for searchdate in dates_of_search)

def find_geographic(my_tweet, geography_of_search):
    """tests if my_tweet is in a given geographic langitude-latitude"""
    geography = my_tweet[3]
    if not geography == None:
        return any(geography in geography_of_search)

def find_hashtag(my_tweet, hashtag_of_search):
    """tests if some hashtags are in my_tweet"""
    hashtag_list = my_tweet[4]
    return any(hashtag in hashtag_of_search for hashtag in hashtag_list)

def mood(list_of_tweets):
       """mood detection: 
            should be improved in the future to go and read the positive / negative mood words from a vocabolary."""
       good_words = ['gut', 'besser','ja','gluecklich'] #fill up with other good_words of interest
       bad_words = ['schlecht', 'nein','traurig','mist','nicht'] #fill up with other bad_words of interest
       good_list, hit_rate_good = statistic(good_words, list_of_tweets)
       bad_list, hit_rate_bad = statistic(bad_words, list_of_tweets)
       return (good_list, hit_rate_good) , (bad_list, hit_rate_bad)

# uses the find_words function to make statistics, lists of the different votings
def study_tweets(list_of_mytweets):
       # using the different find functions:

       #find all tweets in german
       #my_language = 'german'
       #my_german_tweetlist = filter(lambda my_tweet: find_language(my_tweet, my_language),list_of_mytweets)

       # find the period 1 week before and after the election
       date_of_election = date(2013, 9, 22)
       period_of_search = [date(2013, 9, day) for day in range(15,30)]
       placeOfDateElection_in_PeriodOfSearch = 7
       my_election_periodlist = filter(lambda my_tweet: find_date(my_tweet, period_of_search),list_of_mytweets)
       my_election_period_before = filter(lambda my_tweet: find_date(my_tweet, period_of_search[:placeOfDateElection_in_PeriodOfSearch]),list_of_mytweets)
       my_election_period_after = filter(lambda my_tweet: find_date(my_tweet, period_of_search[placeOfDateElection_in_PeriodOfSearch:]),list_of_mytweets)
       
       #different voting questions
       #Wehrpflicht
       wehrpflicht_searchwords = ['Gsoa','Wehrpflicht']
       wehrpflicht_list, hit_rate_wehrpflicht = statistic(wehrpflicht_searchwords,my_election_periodlist)
       (hit_rate_goodmood_wehrfplicht, hit_rate_good_wehrpflicht) , (hit_rate_badmood_wehrfplicht, hit_rate_bad_wehrpflicht) = mood(wehrpflicht_list)
       wehrpflicht_data = (wehrpflicht_list, hit_rate_wehrpflicht, hit_rate_goodmood_wehrfplicht, hit_rate_badmood_wehrfplicht)

       #Epidemiegesetz
       epidemie_searchwords = ['Epidemie','Impfung', 'Impfzwang']
       epidemie_list, hit_rate_epidemie = statistic(epidemie_searchwords,my_election_periodlist)
       (hit_rate_goodmood_epidemie, hit_rate_good_epidemie) , (hit_rate_badmood_epidemie, hit_rate_bad_epidemie) = mood(epidemie_list)
       epidemie_data = (epidemie_list, hit_rate_epidemie, hit_rate_goodmood_epidemie, hit_rate_badmood_epidemie)

       #Tankstellenoeffnungszeiten
       tankstellen_searchwords = ['Tankstellen','ffnungszeiten', 'Arbeitsgesetz']
       tankstellen_list, hit_rate_tankstellen = statistic(tankstellen_searchwords,my_election_periodlist)
       (hit_rate_goodmood_tankstellen, hit_rate_good_tankstellen) , (hit_rate_badmood_tankstellen, hit_rate_bad_tankstellen) = mood(tankstellen_list)
       tankstellen_data = (tankstellen_list, hit_rate_tankstellen, hit_rate_goodmood_tankstellen, hit_rate_badmood_tankstellen)
       
       return wehrpflicht_data, epidemie_data, tankstellen_data


#for testing my program I made a sample of how the download of the tweets should look like
list_of_mytweets = [('epidemiegestz gut', 'english', datetime(2013,9,22,12,35), 'myGeographic', ['#abs13', '#hashtag']),
                    ('wehrpflicht ist schlecht', 'german', datetime(2013,9,19,13,26), 'myGeographic', ['#hashtag1', '#hashtag2']),
                    ('Impfung abstimmung nein', 'german', datetime(2013,9,26,13,26), 'myGeographic', ['#hashtag1', 'hashtag2']),
                    ('nichts','german', datetime(2013,9,23, 12,45,12), 'myGeographic', ['#hashtag1', '#hashtag2']),
                    ('Wehrpflichtersatz ja','german', datetime(2013,9,16, 12,45,12), 'myGeographic', ['hashtag1', 'hashtag2']),
                    ('gsOA nein','german', datetime(2014,9,23, 12,45,12), 'myGeographic', ['hashtag1', 'hashtag2'])]
#wehrpflicht_data, epidemie_data, tankstellen_data = study_tweets(list_of_mytweets)
#print 'Wehrpflicht:\n', wehrpflicht_data,'\n', 'Epidemie:\n', epidemie_data, '\n', 'Tankstellen:\n', tankstellen_data


################################################################
#  Full run
################################################################

if __name__ == '__main__':
    fetch_tweets(10)
    wehrpflicht_data, epidemie_data, tankstellen_data = study_tweets(tweet_list)
    print 'Wehrpflicht:\n', wehrpflicht_data,'\n', 'Epidemie:\n', epidemie_data, '\n', 'Tankstellen:\n', tankstellen_data
